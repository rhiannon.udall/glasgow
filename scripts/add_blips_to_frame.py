#!/usr/bin/env python3

import sys
import lal
import numpy
import scipy
import gengli
from gstlal import psd
from gengli import utils
from gwpy.timeseries import TimeSeries
from lal.utils import CacheEntry

class BlipGlitch(object):
	def __init__(self, psdfile, srate=4096, flow=30, fhighpass=250, ifo = 'H1'):
		self.srate = srate
		self.flow = flow
		self.fhighpass = fhighpass
		self.generator = gengli.glitch_generator(ifo)
		g_raw = self.generator.get_glitch(1,self.srate, fhigh=250)
		glitch_fourier_length = len(g_raw) // 2 + 1
		self.deltaF = 0.5 / (1./4096) / (glitch_fourier_length-1)
		psddoc = psd.read_psd(psdfile)
		self.psd_data = psd.interpolate_psd(psddoc[ifo], self.deltaF)

	def random_glitch(self, snr_low = 8, snr_high = 100):
		snr = numpy.random.uniform(snr_low, snr_high)
		glitch= self.generator.get_glitch(1, self.srate, snr=snr)
		window = scipy.signal.windows.tukey(glitch.shape[-1], alpha= 0.5, sym=True)
		glitch *= window
		glitch_fft = numpy.fft.rfft(glitch, axis=-1)
		ids_kill = numpy.where(numpy.fft.rfftfreq(int(len(glitch)*self.srate/4096.), 1./self.srate) < self.flow)[0]
		glitch_fft[...,ids_kill] = 0.
		colored_glitch = numpy.fft.irfft(glitch_fft*numpy.sqrt(self.psd_data.data.data[:len(glitch_fft)]), axis = -1)
		order = 3
		normal_cutoff = self.fhighpass/(0.5*self.srate)
		b, a = scipy.signal.butter(order, normal_cutoff, btype='low', analog=False)
		colored_glitch = scipy.signal.filtfilt(b, a, colored_glitch, axis = -1)
		return colored_glitch

class FrameData(object):
	def __init__(self, filename, channel = 'H1:FAKE'):
		self.name = filename.split('/')[-1][:-4]
		self.data = TimeSeries.read(filename, channel)
		self.epoch = self.data.t0.value
		self.deltaT = self.data.dt.value
		self.length = len(self.data.value)
		self.start = int(filename.split('-')[-2])
		self.end = self.start + int(filename.split('-')[-1].split('.')[0])

	def inject(self, timeseries, time):
		idx = int((time - self.epoch) / self.deltaT)
		# Add buffer near end
		if idx + len(timeseries) + 1000 < self.length:
			self.data.value[idx:idx + len(timeseries)] += timeseries

	def write_frame(self, name = None, output_dir = 'new_frames/'):
		if name is None:
			name = self.name
		self.data.write(output_dir + name + '.gwf')

glitch = BlipGlitch(sys.argv[1])

cache = list(map(CacheEntry, open('gaussian_frames.cache')))
frame_list = [entry.path for entry in cache]

frame_num = 0
time = 1000000150
num_injections = 1000
inj_performed = 0
times = []

while inj_performed < num_injections:
	frame = FrameData(frame_list[frame_num])
	while frame.start < time < frame.end:
		time += 100
		inj_performed += 1
		colored_glitch = glitch.random_glitch()
		frame.inject(colored_glitch, time)
		times.append(time)
	frame.write_frame()
	frame_num += 1

print(times)
