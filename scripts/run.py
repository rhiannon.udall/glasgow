import torch
from torch import nn
import torch.nn.functional as F
from torch.autograd import Variable
from torchsummary import summary
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
import numpy as np
from timeit import default_timer as timer
from tqdm import tqdm

from glasgow.model import ConvNet
from glasgow.model import ModelTrainer


# Custom Model 

model = ConvNet()

# Model Trainer called with default arguments

trainer = ModelTrainer()
training_data, testing_data = trainer.prepare_datasets()


# Steps called for training, loading and evaluating the model

trainer.train_model(train_dataset = training_data, test_dataset = testing_data, epochs = 10)
trainer.load_model()
trainer.eval_model(test_data = testing_data)

